
import requests

from bs4 import BeautifulSoup
from requests.api import head
import time

URL = "https://mofa.gov.bd/site/view/service_box_items/PRESS%20RELEASES/"

r = requests.get(URL)


soup = BeautifulSoup(r.content, 'html5lib')

all_links = []

links = soup.find_all('a')

for file in links:

    link = file['href']
    all_links.append(link)


page = []  # empty list to store links which contains page
for x in all_links:

    # find function returns -1 when find value doesnt match in the string
    val = (x.find('page='))
    if val > -1:    # checking if the link matched "page="
        page.append(x)  # inserting the link in the list

    else:
        val = (x.find('pgn='))
        if val > -1:
            page.append(x)


all_pages = []

for y in page:
    content = y.split("page=")
    data = content[1].split("&")
    all_pages.append(data[0])

all_pages.sort(key=int)

val = all_pages[len(all_pages)-1]
val = int(val)
crawal_links = []
data_table = []
x = 1
while(x <= val):  # change here to select number of pages to crawl by default it is set to last page
    if x == 1:
        URL = "https://mofa.gov.bd/site/view/service_box_items/PRESS%20RELEASES/"
    else:
        URL = f'https://mofa.gov.bd/site/view/service_box_items/PRESS%20RELEASES/?page={x}&rows=20'

    x = x+1
    r = requests.get(URL)

    soup = BeautifulSoup(r.content, 'html5lib')

    table = soup.find("table")

    trs = table.find_all("tr")

    for tr in trs:
        tds = tr.find_all("td")
        crawal_links.append(tds[1])
        data_table.append(str(tds[0].contents[0]) + "," +
                          str(tds[1].contents[1].contents[0])+"," +
                          str(tds[2].contents[0]) + "\n")

    # print(crawal_links)
    final_crawl_links = []

    for eachLink in crawal_links:
        data = str(eachLink)
        val = data.split('"')
        final_crawl_links.append(val[3])

    i = 1
    j = 1
    for eachLink in final_crawl_links:
        data = str(eachLink)
        URL = "https://mofa.gov.bd" + data
        s = 5
        r = requests.get(URL, time.sleep(s), timeout=360)

        soup = BeautifulSoup(r.content, 'html5lib')
        page_content = soup.find("div", {"id": "printable_area"})

        images = page_content.find_all('img')
        for image in images:

            link = image['src']
            if link.endswith('.jpeg'):

                if link.startswith("//"):

                    link = "http:"+link

                    with open(str(i) + ".jpeg", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)

                elif link.startswith("/"):

                    link = "https://mofa.gov.bd"+link

                    with open(str(i) + ".jpeg", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)

                else:
                    with open(str(i) + ".jpeg", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)

            elif link.endswith('.png'):

                if link.startswith("//"):

                    link = "http:"+link

                    with open(str(i) + ".png", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)
                elif link.startswith("/"):

                    link = "https://mofa.gov.bd"+link

                    with open(str(i) + ".png", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)

                else:
                    with open(str(i) + ".png", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)

            elif link.endswith('.bmp'):

                if link.startswith("//"):

                    link = "http:"+link

                    with open(str(i) + ".bmp", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)
                elif link.startswith("/"):

                    link = "https://mofa.gov.bd"+link

                    with open(str(i) + ".bmp", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)

                else:
                    with open(str(i) + ".bmp", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)

            elif link.endswith('.jpg'):

                if link.startswith("//"):

                    link = "http:"+link

                    with open(str(i) + ".jpg", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)
                elif link.startswith("/"):

                    link = "https://mofa.gov.bd"+link

                    with open(str(i) + ".jpg", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)
                else:
                    with open(str(i) + ".jpg", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s), timeout=360)
                            f.write(im.content)
                            print('Done: ', i)
                        except:
                            im = requests.get(
                                link, time.sleep(s), timeout=360, verify=False)
                            f.write(im.content)
                            print('Done: ', i)
        i = i+1

        links = page_content.find_all('a')
        for file in links:

            link = file['href']
            # pdf file downloader
            if link.endswith('.pdf'):

                if link.startswith("//"):

                    link = "http:"+link

                    with open(str(j) + ".pdf", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s))
                            f.write(im.content)
                            print('Done: ', j)
                        except:
                            im = requests.get(
                                link, time.sleep(s), verify=False)
                            f.write(im.content)
                            print('Done: ', j)

                elif link.startswith("/"):

                    link = "https://mofa.gov.bd"+link
                    with open(str(j) + ".pdf", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s))
                            f.write(im.content)
                            print('Done: ', j)
                        except:
                            im = requests.get(
                                link, time.sleep(s), verify=False)
                            f.write(im.content)
                            print('Done: ', j)

                else:
                    with open(str(j) + ".pdf", 'wb') as f:
                        try:
                            im = requests.get(link, time.sleep(s))
                            f.write(im.content)
                            print('Done: ', j)
                        except:
                            im = requests.get(
                                link, time.sleep(s), verify=False)
                            f.write(im.content)
                            print('Done: ', j)
            j = j+1

    print(f'done page no: {x-1}')


with open("asd.csv", "w", encoding="UTF-8") as f:

    for row in data_table:
        f.write(row)
